$(document).ready(function () {
let options = { 
    mobiles: ['images/mobiles/mobile1.jpg', 'images/mobiles/mobile2.jpg', 'images/mobiles/mobile3.jpg', 'img/mobiles/mobile4.jpg', 'img/mobiles/mobile5.jpg'],
    tablets: ['images/tablets/tablet1.jpg', 'images/tablets/tablet2.jpg', 'images/tablets/tablet3.jpg', 'img/tablets/tablet4.jpg'],
    sports: ['images/sports/sports1.jpg', 'images/sports/sports2.jpg', 'images/sports/sports3.jpg', 'img/sports/sports4.jpg'],
    tv: ['images/tv/tv1.jpg', 'images/tv/tv2.jpg', 'images/tv/tv3.jpg', 'img/tv/tv4.jpg'],
    Books:['images/books/books1.jpg', 'images/books/books2.jpg'],
    LED : ['images/led/led1.jpg', 'images/led/led2.jpg', 'images/led/led3.jpg', 'img/led/led4.jpg']
};
$(".category").click(function () {
    let id=$(this).data('id');
        $(".category").removeClass("category_active");
        $(this).addClass("category_active");
        if (id === "mobiles"){
            $(".carousel_item").remove();
            for (let i = 0; i < options.mobiles.length; i++){
                let image = `<div class="carousel-item carousel_item">
                              <img class="" src="images/mobiles/mobiles${(i+1)}.jpg"> 
                            </div>`;
                $(".carousel_inner").append(image);
            }
        let x = document.getElementsByClassName('carousel_item');
        x[0].classList.add("active");
        }
        else if (id === "sports"){
            $(".carousel_item").remove();
            for (let i = 0; i < options.sports.length; i++){
                let image = `<div class="carousel-item carousel_item">
                              <img class="" src="images/sports/sports${(i+1)}.jpg"> 
                            </div>`;
                $(".carousel_inner").append(image);
            }
        let x = document.getElementsByClassName('carousel_item');
        x[0].classList.add("active");
        }

        else if (id === "tablets"){
            $(".carousel_item").remove();
            for (let i = 0; i < options.tablets.length; i++){
                let image = `<div class="carousel-item carousel_item">
                              <img class="" src="images/tablets/tablet${(i+1)}.jpg"> 
                            </div>`;
                $(".carousel_inner").append(image);
            }
            let x = document.getElementsByClassName('carousel_item');
            x[0].classList.add("active");
        }

        else if (id === "led"){
            $(".carousel_item").remove();
            for (let i = 0; i < options.LED.length; i++){
                 let image = `<div class="carousel-item carousel_item"> 
                                        <img class="" src="images/led/led${(i+1)}.jpg" >
                                    </div>`;
                $(".carousel_inner").append(image);
            }
            let x = document.getElementsByClassName('carousel_item');
            x[0].classList.add("active");
        }
        else if (id === "tv"){
            $(".carousel_item").remove();
            for (let i = 0; i < options.tv.length; i++){
                 let image = `<div class="carousel-item carousel_item"> 
                                        <img class="" src="images/tv/tv${(i+1)}.jpg" >
                                    </div>`;
                $(".carousel_inner").append(image);
            }
            let x = document.getElementsByClassName('carousel_item');
            x[0].classList.add("active");
        }
        else if (id === "books"){
            $(".carousel_item").remove();
            for (let i = 0; i < options.Books.length; i++){
                 let image = `<div class="carousel-item carousel_item"> 
                                        <img class="" src="images/books/books${(i+1)}.jpg" >
                                    </div>`;
                $(".carousel_inner").append(image);
            }
            let x = document.getElementsByClassName('carousel_item');
            x[0].classList.add("active");
        }
    });


});